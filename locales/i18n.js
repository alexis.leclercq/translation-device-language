import memoize from "lodash.memoize";
import i18n from 'i18n-js';
import * as Localization from 'expo-localization';

import fr from './fr.json';
import en from './en.json';

i18n.defaultLocale = 'en';
i18n.fallbacks = true;
i18n.translations = { fr, en };
i18n.locale = Localization.locale;

export const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key)
);

export let locale = i18n.locale;